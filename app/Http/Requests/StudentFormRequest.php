<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {


        $rules = [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'email' => [
                'required',
                'email',
                'max:255',
                'unique:students,email',
            ],
            'gender' => [
                'required',
                'string',

            ],
            'course' => [
                'required',

            ],
            'hobby' => [
                'required',
                'string',
            ]



        ];

        return $rules;
    }
    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'email.required' => 'Email ID is required',
            'gender.required' =>'Gender is required',
            'course.required' =>'Course is required',
            'hobby.required' =>'hobby is rrquired'
        ];
    }
}
