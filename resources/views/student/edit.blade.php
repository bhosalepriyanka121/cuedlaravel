@extends('student.layout')
@section('content')
<br> <br>
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Add Student</h4>
                </div>
                <div class="card-body">

                    <form action="{{ url('update/'.$table->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group mb-3">
                            <label for="">Student Name</label>
                            <input type="text" name="name" value="{{$table->name}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Student Email</label>
                            <input type="text" name="email" value="{{$table->email}}" class="form-control">
                        </div>
                        <div>
                            <label for="">Student Gender</label>
                            <br><input type="radio" name="gender" value="{{$table->gender}}" checked>Male <br>
                            <input type="radio" name="gender" value="{{$table->gender}}" checked>Female
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Student Course</label>
                            <input type="text" name="course" value="{{$table->course}}" class="form-control">
                        </div>
                        <div>
                            <label for="">Student hobbies</label>
                            <br> <br><input type="checkbox" name="hobby" value="sports" {{ $table->hobby=="sports"? 'checked':'' }}>sports
                            <input type="checkbox" name="hobby" value="music" {{ $table->hobby=="music"? 'checked':'' }}>music
                            <input type="checkbox" name="hobby" value="reading" {{ $table->hobby=="reading"? 'checked':'' }}>reading
                        </div>

                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">Update Student</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection