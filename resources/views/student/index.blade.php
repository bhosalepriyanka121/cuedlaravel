@extends('student.layout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>
                        <center>How to Fetch data in Laravel 8</center>
                    </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Course</th>
                                <th>hobby</th>

                                <th>Edit</th>
                                <th>create</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($table as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->gender }}</td>
                                <td>{{ $item->course }}</td>
                                <td>{{ $item->hobby }}</td>
                                <td>
                                    <a href="{{ url('edit/'.$item->id)}}" class="btn btn-primary btn-sm">Edit</a>
                                </td>

                                <td>
                                    <a href="{{ url('create')}}" class="btn btn-dark btn-sm">Create</a>
                                </td>

                                <td>
                                    <!-- <a href="" class="btn btn-danger btn-sm">Delete</a> -->
                                    <form action="{{ url('delete/'.$item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection