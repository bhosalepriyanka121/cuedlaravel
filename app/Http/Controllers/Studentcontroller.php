<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentFormRequest;
use Illuminate\Http\Request;
use App\Models\Student;

class Studentcontroller extends Controller
{
    public function create()
    {
        //Comment added
        return view('student.create');
    }
    public function store(StudentFormRequest $request)
    {
        $data = $request->validated();
        Student::create($data);
        return redirect('/create')->with('message', 'Student Added Successfully');
    }



    public function index()
    {
        $table = Student::all();
        return view('student.index', compact('table'));
    }

    public function edit($id)
    {
        $table = Student::find($id);

        return view('student.edit', compact('table'));
        //return redirect('/index');
    }

    public function update(Request $request, $id)
    {
        $table = Student::find($id);
        $table->name = $request->input('name');
        $table->email = $request->input('email');
        $table->gender = $request->input('gender');
        $table->course = $request->input('course');
        $table->hobby = $request->input('hobby');

        $table->update();
        return redirect('/index');
    }

    public function delete($id)
    {
        $table = Student::find($id);

        $table->delete();
        return redirect('/index');
    }
}
