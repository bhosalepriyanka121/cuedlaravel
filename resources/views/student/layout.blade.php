<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Student Record</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
   <style>
      .button {
         margin-top: -10%;
         display: block;
         width: 114px;
         height: 42px;
         background: #4e62af;
         padding: 10px;
         text-align: center;
         border-radius: 5px;
         color: white;
         font-weight: bold;
         line-height: 25px;
         margin-left: 71%;

      }
   </style>
</head>

<body>
   <div class="container">
      @yield('content')
   </div>
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" type="text/js"></script>
</body>

</html>