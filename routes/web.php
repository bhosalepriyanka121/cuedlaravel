<?php

use App\Http\Controllers\Studentcontroller;
use Illuminate\Support\Facades\Route;

/*
|-----------------------------------------------------------------------------------
|-------------
| Web Routes
|----------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('create',[Studentcontroller::class ,'create']);
Route::post('create',[Studentcontroller::class,'store']);
Route::get('index',[Studentcontroller::class,'index']);
Route::get('edit/{id}',[Studentcontroller::class,'edit']);
Route::put('update/{id}',[Studentcontroller::class,'update']);
Route::delete('delete/{id}',[Studentcontroller::class,'delete']);
