@extends('student.layout')
@section('content')
<br> <br>
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-6">
            @if (session('message'))
            <h5 class="alert alert-success">{{ session('message') }}</h5>
            @endif

            @if ($errors->any())
            <ul class="alert alert-warning">
                @foreach ($errors->all() as $error)
                <li> {{ $error }}</li>
                @endforeach
            </ul>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Add Student</h4>
                </div>
                <div class="card-body">

                    <form action="{{ url('create')}}" method="POST">
                        @csrf

                        <div class="form-group mb-3">
                            <label for="">Student Name</label>
                            <input type="text" name="name" class="form-control">

                        </div>
                        <div class="form-group mb-3">
                            <label for="">Student Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                        <div>
                            <label for="">Student Gender</label><br>
                            <input type="radio" id="Male" name="gender" value="Male">
                            <label for="Male">Male</label><br>
                            <input type="radio" id="Female" name="gender" value="Female">
                            <label for="Female">Female</label>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Student Course</label>
                            <input type="text" name="course" class="form-control">
                        </div>
                        <div>
                            <label for=""> Student hobbies</label> <br>
                            <input type="checkbox" name="hobby" value="sports">
                            <label for="hobby"> sports</label>
                            <input type="checkbox" name="hobby" value="music">
                            <label for="hobby">music</label>
                            <input type="checkbox" name="hobby" value="reading">
                            <label for="hobby">reading</label><br><br>
                        </div>


                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">Save Student</button>
                        </div>
                        <a class="button" href="/index">Index Page</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection